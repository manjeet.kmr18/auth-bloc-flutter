import 'package:firebase_bloc_demo/auth/auth_bloc/auth_bloc.dart';
import 'package:firebase_bloc_demo/login/login_screen.dart';
import 'package:firebase_bloc_demo/repositories/user_repository.dart';
import 'package:firebase_bloc_demo/signup/signup_bloc/signup_bloc.dart';
import 'package:firebase_bloc_demo/widgets/custom_button.dart';
import 'package:firebase_bloc_demo/widgets/loginbottomtext.dart';
import 'package:firebase_bloc_demo/widgets/verticalText.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SignUpForm extends StatefulWidget {
  UserRepository userRepository;
  SignUpForm({@required this.userRepository});

  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  TextEditingController _emailctrl = TextEditingController();

  TextEditingController _passctrl = TextEditingController();

  SignupBloc signupBloc;

  @override
  void initState() {
    signupBloc = BlocProvider.of<SignupBloc>(context);
    super.initState();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocListener<SignupBloc, SignupState>(
      listener: (context, state) {
        if (state is UserSignupFailed) {
          Scaffold.of(context)
            ..removeCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Register Failure'),
                    Icon(Icons.error),
                  ],
                ),
                backgroundColor: Color(0xff9CC3D5FF),
              ),
            );
        }

        if (state is SignupLoading) {
          Scaffold.of(context)
            ..removeCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Registering...'),
                    CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                    )
                  ],
                ),
                backgroundColor: Color(0xff9CC3D5FF),
              ),
            );
        }

        if (state is UserSignupSucessfully) {
          BlocProvider.of<AuthBloc>(context).add(
            AuthLoggedIn(),
          );
        }
      },
      child: BlocBuilder<SignupBloc, SignupState>(
        builder: (context, state) {
          return _signupformbody(context, state);
        },
      ),
    );
  }

  Widget _signupformbody(context, state) {
    return Column(
      children: <Widget>[
        VerticalText(
          logText: 'Sign Up',
          logdesc: 'We can start something new SignUp',
        ),
        Padding(
          padding: const EdgeInsets.only(right: 30, left: 30),
          child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  TextFormField(
                    controller: _emailctrl,
                    keyboardType: TextInputType.emailAddress,
                    // validator: (value) {
                    //   if (value.isEmpty ||
                    //       !RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                    //           .hasMatch(value)) {
                    //     return 'Enter a valid email!';
                    //   }
                    //   return null;
                    // },
                    decoration: InputDecoration(
                      labelText: 'Email',
                      labelStyle: TextStyle(color: Colors.white),
                      border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        borderSide:
                            new BorderSide(color: Colors.white, width: 5.0),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    controller: _passctrl,
                    obscureText: true,
                    // validator: (value) {
                    //   if (value.isEmpty) {
                    //     return 'Please enter some text';
                    //   }
                    //   if (value.length < 8) {
                    //     return 'Must be more than 8 charater';
                    //   }
                    // },
                    decoration: InputDecoration(
                      labelText: 'Password',
                      labelStyle: TextStyle(color: Colors.white),
                      border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        borderSide:
                            new BorderSide(color: Colors.white, width: 5.0),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  CustomButtonLogin(
                    buttomtext: 'SignUp',
                    buttonicon: Icons.forward,
                    onTap: () async {
                      signupBloc
                        ..add(
                          SignUpButtonPressed(
                              email: _emailctrl.text, password: _passctrl.text),
                        );
                    },
                  ),
                  LoginBottomText(
                    firsttext: 'Already have account?',
                    buttontext: 'SignIn',
                    navLink: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => LoginScreen(
                            userRepository: widget.userRepository,
                          ),
                        ),
                      );
                    },
                  ),
                ],
              )),
        )
      ],
    );
  }

  @override
  void dispose() {
    _emailctrl.dispose();
    _passctrl.dispose();
    signupBloc = BlocProvider.of<SignupBloc>(context);
    super.dispose();
  }
}

import 'package:firebase_bloc_demo/repositories/user_repository.dart';
import 'package:firebase_bloc_demo/signup/signup_bloc/signup_bloc.dart';
import 'package:firebase_bloc_demo/signup/signup_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SignupScreen extends StatelessWidget {
  UserRepository userRepository;
  SignupScreen({@required this.userRepository});

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [Colors.blueGrey, Colors.lightBlueAccent]),
        ),
        child:
        BlocProvider(
          create: (context) => SignupBloc(userRepository: userRepository),
          child: SignUpForm(userRepository: userRepository),
        ),),
    );

  }
}

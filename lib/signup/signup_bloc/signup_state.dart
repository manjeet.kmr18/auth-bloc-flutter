part of 'signup_bloc.dart';

abstract class SignupState extends Equatable {}

class SignupInitial extends SignupState {
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}

class SignupLoading extends SignupState {
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}

class UserSignupSucessfully extends SignupState {

  User user;
  UserSignupSucessfully(this.user);

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}

class UserSignupFailed extends SignupState{

  String message;
  UserSignupFailed(this.message);

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();

}

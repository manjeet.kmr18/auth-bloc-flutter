part of 'signup_bloc.dart';


abstract class SignupEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class SignUpButtonPressed extends SignupEvent {

  String email, password;

  SignUpButtonPressed({this.email, this.password});

  @override
  List<Object> get props => [email, password];

}
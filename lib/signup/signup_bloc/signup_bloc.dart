import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_bloc_demo/repositories/user_repository.dart';
import 'package:flutter/cupertino.dart';

part 'signup_event.dart';
part 'signup_state.dart';

class SignupBloc extends Bloc<SignupEvent, SignupState> {

  UserRepository userRepository;
  SignupBloc({@required UserRepository userRepository }) : super(null){
    this.userRepository = userRepository;
  }


  @override
  Stream<SignupState> mapEventToState(
    SignupEvent event,
  ) async* {
    if (event is SignUpButtonPressed) {
      yield SignupLoading();
      try {
        var user = await userRepository.signUpWithCredentials(
            event.email, event.password);
        yield UserSignupSucessfully(user);
      } catch (e) {
        yield UserSignupFailed(e.toString());
      }
    }
  }
}

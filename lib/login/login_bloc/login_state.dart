part of 'login_bloc.dart';

abstract class LoginState extends Equatable {}

class LoginInitial extends LoginState {
  @override
  List<Object> get props => throw UnimplementedError();
}

class LoginLoadingState extends LoginState {
  @override
  List<Object> get props => throw UnimplementedError();
}

class LoginSuccessState extends LoginState {
  User user;

  LoginSuccessState(@required this.user);
  @override
  List<Object> get props => throw UnimplementedError();
}
class LoginFailState extends LoginState {
  String message;
  LoginFailState(@required this.message);

  @override
  List<Object> get props => throw UnimplementedError();
}

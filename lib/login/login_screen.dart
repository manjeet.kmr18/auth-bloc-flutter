import 'package:firebase_bloc_demo/login/login_bloc/login_bloc.dart';
import 'package:firebase_bloc_demo/login/login_form.dart';
import 'package:firebase_bloc_demo/repositories/user_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginScreen extends StatelessWidget {
  final UserRepository userRepository;

  const LoginScreen({@required this.userRepository});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [Colors.blueGrey, Colors.lightBlueAccent]),
        ),
        child: BlocProvider<LoginBloc>(
          create: (context) => LoginBloc(userRepository: userRepository),
          child: LoginForm(
            userRepository: userRepository,
          ),
        ),
      ),
    );
  }
}

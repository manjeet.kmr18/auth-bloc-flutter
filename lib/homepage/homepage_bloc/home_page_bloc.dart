import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_bloc_demo/repositories/user_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

part 'home_page_event.dart';
part 'home_page_state.dart';

class HomePageBloc extends Bloc<HomePageEvent, HomePageState> {
  UserRepository userRepository;


  HomePageBloc({@required this.userRepository}) : super(LogOutInitial()){
  this.userRepository = userRepository;
  }

  @override
  Stream<HomePageState> mapEventToState(
    HomePageEvent event,
  ) async* {
    if (event is LogOutEvent) {
      print("Logout");
      userRepository.signOut();
      yield LogOutSuccessState();
    }
  }
}

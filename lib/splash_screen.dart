import 'dart:async';
import 'package:firebase_bloc_demo/repositories/user_repository.dart';
import 'package:flutter/material.dart';

import 'login/login_screen.dart';

class SplashScreen extends StatefulWidget {
  UserRepository userRepository;

   SplashScreen({@required this.userRepository});

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  // @override
  // void initState() {
  //   super.initState();
  //   Timer(
  //       Duration(seconds: 2),
  //       () => Navigator.of(context).pushReplacement(
  //           MaterialPageRoute(builder: (BuildContext context) => LoginScreen(userRepository: widget.userRepository,))));
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [Colors.blueGrey, Colors.lightBlueAccent]),
        ),
        child: Center(child: Text('Welcome To Bloc Demo')),
      ),
    );
  }
}

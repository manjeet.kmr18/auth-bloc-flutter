import 'package:firebase_bloc_demo/homepage/homepage.dart';
import 'package:firebase_bloc_demo/login/login_screen.dart';
import 'package:firebase_bloc_demo/repositories/user_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'auth_bloc/auth_bloc.dart';

class AuthPage extends StatelessWidget {
  UserRepository userRepository;

  AuthPage({@required this.userRepository});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(
      builder: (context, state) {
        if (state is AuthenticatedState) {
          return HomeScreen(
              user: state.user,
              // userRepository: userRepository
          );
        }
        else if (state is UnauthenticatedState) {
          return LoginScreen(
            userRepository: userRepository,
          );
        }
      },
    );
  }
}

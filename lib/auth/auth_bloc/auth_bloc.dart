import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_bloc_demo/repositories/user_repository.dart';
import 'package:meta/meta.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final UserRepository _userRepository;
  AuthBloc({UserRepository userRepository})
      : _userRepository = userRepository,
        super(AuthInitial());

  // UserRepository _userRepository =  new UserRepository();

  @override
  Stream<AuthState> mapEventToState(
    AuthEvent event,
  ) async* {
    if (event is Authinit) {
      yield* _mapAuthinitToState();
    } else if (event is AuthLoggedIn) {
      yield* _mapAuthLoggedInToState();
    } else if (event is AuthLoggedOut) {
      yield* _mapAuthLoggedOutInToState();
    }
  }

  Stream<AuthState> _mapAuthinitToState() async* {
    try {
      var isSignedIn = await _userRepository.isSignedIn();
      if (isSignedIn) {
        final user = await _userRepository.getUser();
        yield AuthenticatedState(user);
      } else {
        yield UnauthenticatedState();
      }
    } catch (e) {
      yield UnauthenticatedState();
    }
  }

  Stream<AuthState> _mapAuthLoggedInToState() async* {
    yield AuthenticatedState(await _userRepository.getUser());
  }

  Stream<AuthState> _mapAuthLoggedOutInToState() async* {
    yield UnauthenticatedState();
    _userRepository.signOut();
  }
}

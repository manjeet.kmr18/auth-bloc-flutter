part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];
}

class AuthInitial extends AuthState {}

class AuthenticatedState extends AuthState {
  User user;

  AuthenticatedState(@required this.user);

  @override
  List<Object> get props => [user];
}

class UnauthenticatedState extends AuthState {}

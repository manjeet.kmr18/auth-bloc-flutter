import 'package:flutter/material.dart';

class CustomButtonLogin extends StatefulWidget {
  final String buttomtext;
  final Function onTap;
  final IconData buttonicon;

  const CustomButtonLogin(
      {Key key, this.buttomtext, this.onTap, this.buttonicon})
      : super(key: key);

  @override
  _CustomButtonLoginState createState() => _CustomButtonLoginState();
}

class _CustomButtonLoginState extends State<CustomButtonLogin> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20, left: 150),
      child: Container(
        alignment: Alignment.bottomRight,
        height: 50,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.blue[300],
              blurRadius: 10.0,
              spreadRadius: 1.0,
              offset: Offset(
                5.0,
                5.0,
              ),
            ),
          ],
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
        ),
        child: FlatButton(
          color: Colors.white,
          onPressed: widget.onTap,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                widget.buttomtext,
                style: TextStyle(
                  color: Colors.lightBlueAccent,
                  fontSize: 15,
                ),
              ),
              Icon(
                widget.buttonicon,
                color: Colors.lightBlueAccent,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

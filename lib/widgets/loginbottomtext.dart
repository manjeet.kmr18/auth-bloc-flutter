import 'package:flutter/material.dart';

class LoginBottomText extends StatefulWidget {
  final String firsttext;
  final String buttontext;
  final Function navLink;

  LoginBottomText({Key key, this.firsttext, this.buttontext, this.navLink})
      : super(key: key);

  @override
  _LoginBottomTextState createState() => _LoginBottomTextState();
}

class _LoginBottomTextState extends State<LoginBottomText> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Container(
        child: Row(
          children: <Widget>[
            Text(
              widget.firsttext,
              style: TextStyle(
                fontFamily: 'Montserrat',
                color: Colors.white,
              ),
            ),
            SizedBox(width: 5.0),
            InkWell(
              onTap: widget.navLink,
              child: Text(
                widget.buttontext,
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'Montserrat',
                    fontWeight: FontWeight.bold,
                    decoration: TextDecoration.underline),
              ),
            )
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

class VerticalText extends StatefulWidget {
  final String logText, logdesc;

  const VerticalText({Key key, this.logText, this.logdesc}) : super(key: key);

  @override
  _VerticalTextState createState() => _VerticalTextState();
}

class _VerticalTextState extends State<VerticalText> {
  GlobalKey _keyform = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 60, left: 10),
          child: RotatedBox(
              quarterTurns: -1,
              child: Text(
                widget.logText,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 38,
                  fontWeight: FontWeight.w900,
                ),
              )),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 30.0, left: 10.0),
          child: Container(
            height: 200,
            width: 200,
            child: Center(
              child: Text(
                widget.logdesc,
                style: TextStyle(
                  fontSize: 24,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
